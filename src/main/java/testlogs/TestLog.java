package testlogs;

import java.util.Date;
import java.util.List;

public class TestLog {

	private String name;
	private String testRunId;
	private String testId;
	private List<String>failResons;
	private	Date testStartTime;
	private	int testRunTime;
	private	String ciBuildName;
	private	String suiteName;
	private	String env;
	private	String browser;
	private	String browserVersion;

	
	
	private	List<TestStep> testSteps;
	private TestRunStatus testRunStatus;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTestRunId() {
		return testRunId;
	}

	public void setTestRunId(String testRunId) {
		this.testRunId = testRunId;
	}

	public String getTestId() {
		return testId;
	}

	public void setTestId(String testId) {
		this.testId = testId;
	}

	public List<String> getFailResons() {
		return failResons;
	}

	public void setFailResons(List<String> failResons) {
		this.failResons = failResons;
	}

	public Date getTestStartTime() {
		return testStartTime;
	}

	public void setTestStartTime(Date testStartTime) {
		this.testStartTime = testStartTime;
	}

	public int getTestRunTime() {
		return testRunTime;
	}

	public void setTestRunTime(int testRunTime) {
		this.testRunTime = testRunTime;
	}

	public String getCiBuildName() {
		return ciBuildName;
	}

	public void setCiBuildName(String ciBuildName) {
		this.ciBuildName = ciBuildName;
	}

	public String getSuiteName() {
		return suiteName;
	}

	public void setSuiteName(String suiteName) {
		this.suiteName = suiteName;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getBrowserVersion() {
		return browserVersion;
	}

	public void setBrowserVersion(String browserVersion) {
		this.browserVersion = browserVersion;
	}

	public List<TestStep> getTestSteps() {
		return testSteps;
	}

	public void setTestSteps(List<TestStep> testSteps) {
		this.testSteps = testSteps;
	}

	@Override
	public String toString() {
		return "TestLog [name=" + name + ", testRunId=" + testRunId + ", testId=" + testId + ", testStartTime="
				+ testStartTime + ", testRunTime=" + testRunTime + ", ciBuildName=" + ciBuildName + ", suiteName="
				+ suiteName + ", env=" + env + ", browser=" + browser + ", browserVersion=" + browserVersion + "]";
	}
	
	public static void addStep(String stepName){
		TestStep step=new TestStep();
	}
	
	public static class testLogBuilder{
		
		
		
	}

	public TestRunStatus getTestRunStatus() {
		return testRunStatus;
	}

	public void setTestRunStatus(TestRunStatus testRunStatus) {
		this.testRunStatus = testRunStatus;
	}

}
