package testlogs;

public class TestObject {
	
	String testName;
	String testDescription;
	String testStatus;
	String testId;
	String tetsClass;
	String testCategory;
	String testType;
	
	
	
	public TestObject(String testName, String testDescription, String testStatus) {
		super();
		this.testName = testName;
		this.testDescription = testDescription;
		this.testStatus = testStatus;
	}
	public String getTestName() {
		return testName;
	}
	public void setTestName(String testName) {
		this.testName = testName;
	}
	public String getTestDescription() {
		return testDescription;
	}
	public void setTestDescription(String testDescription) {
		this.testDescription = testDescription;
	}
	public String getTestStatus() {
		return testStatus;
	}
	public void setTestStatus(String testStatus) {
		this.testStatus = testStatus;
	}
	public String getTestId() {
		return testId;
	}
	public void setTestId(String testId) {
		this.testId = testId;
	}
	public String getTetsClass() {
		return tetsClass;
	}
	public void setTetsClass(String tetsClass) {
		this.tetsClass = tetsClass;
	}
	public String getTestCategory() {
		return testCategory;
	}
	public void setTestCategory(String testCategory) {
		this.testCategory = testCategory;
	}
	public String getTestType() {
		return testType;
	}
	public void setTestType(String testType) {
		this.testType = testType;
	}
}
