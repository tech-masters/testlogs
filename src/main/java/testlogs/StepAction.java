package testlogs;

public class StepAction {
	
	String description;
	TestRunStatus status;
	String data;
	TestScreenshot screenshot;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public TestRunStatus getStatus() {
		return status;
	}
	public void setStatus(TestRunStatus status) {
		this.status = status;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public TestScreenshot getScreenshot() {
		return screenshot;
	}
	public void setScreenshot(TestScreenshot screenshot) {
		this.screenshot = screenshot;
	}

}
