package testlogs;

import java.util.Date;
import java.util.List;

public class TestStep {
	
	String stepDescription;
	Date stepStartTime;
	int stepRunTime;
	boolean stepStatus;
	TestScreenshot screenshot;
	List<StepAction>stepActions;
	public String getStepDescription() {
		return stepDescription;
	}
	public void setStepDescription(String stepDescription) {
		this.stepDescription = stepDescription;
	}
	public Date getStepStartTime() {
		return stepStartTime;
	}
	public void setStepStartTime(Date stepStartTime) {
		this.stepStartTime = stepStartTime;
	}
	public int getStepRunTime() {
		return stepRunTime;
	}
	public void setStepRunTime(int stepRunTime) {
		this.stepRunTime = stepRunTime;
	}
	public boolean isStepStatus() {
		return stepStatus;
	}
	public void setStepStatus(boolean stepStatus) {
		this.stepStatus = stepStatus;
	}
	public TestScreenshot getScreenshot() {
		return screenshot;
	}
	public void setScreenshot(TestScreenshot screenshot) {
		this.screenshot = screenshot;
	}
	public List<StepAction> getStepActions() {
		return stepActions;
	}
	public void setStepActions(List<StepAction> stepActions) {
		this.stepActions = stepActions;
	}

}
